api = 2
core = 7.x

;============================================
;  ES Starterkit modules
;============================================

; ES Field Storage Sandbox
projects[elasticsearch][type] = module
projects[elasticsearch][download][type] = git
projects[elasticsearch][download][url] = http://git.drupal.org/sandbox/asgorobets/2073151.git
projects[elasticsearch][download][branch] = 7.x-1.x
projects[elasticsearch][subdir] = contrib

;============================================
;  ES Starterkit Features
;============================================

; ES Baseball Display Feature
projects[es_feature_baseball_display][type] = module
projects[es_feature_baseball_display][download][type] = git
projects[es_feature_baseball_display][download][url] = git@bitbucket.org:asgorobets/es-feature-baseball-display.git
projects[es_feature_baseball_display][download][branch] = master
projects[es_feature_baseball_display][subdir] = custom/features

; ES Search API Feature
projects[es_feature_search_api][type] = module
projects[es_feature_search_api][download][type] = git
projects[es_feature_search_api][download][url] = git@bitbucket.org:asgorobets/es-feature-search-api.git
projects[es_feature_search_api][download][branch] = master
projects[es_feature_search_api][subdir] = custom/features

;============================================
;  Contrib modules
;============================================

projects[admin_menu][version] = 3.0-rc4
projects[admin_menu][subdir] = contrib

projects[ctools][version] = 1.3
projects[ctools][subdir] = contrib

projects[views][version] = 3.7
projects[views][subdir] = contrib

projects[module_filter][version] = 1.8
projects[module_filter][subdir] = contrib

projects[devel][version] = 1.3
projects[devel][subdir] = contrib

projects[entity][version] = 1.2
projects[entity][subdir] = contrib

projects[facetapi][version] = 1.3
projects[facetapi][subdir] = contrib

projects[search_api][version] = 1.8
projects[search_api][subdir] = contrib

projects[features][version] = 2.0-rc4
projects[features][subdir] = contrib

projects[features_override][version] = 2.0-beta3
projects[features_override][subdir] = contrib

projects[migrate][version] = 2.x-dev
projects[migrate][subdir] = contrib
projects[migrate][download][type] = git
projects[migrate][download][revision] = 4a5a6e3
projects[migrate][download][branch] = 7.x-2.x

projects[libraries][version] = 2.1
projects[libraries][subdir] = contrib

projects[search_api_elasticsearch][version] = 1.x-dev
projects[search_api_elasticsearch][subdir] = contrib
projects[search_api_elasticsearch][download][type] = git
projects[search_api_elasticsearch][download][revision] = 7586597
projects[search_api_elasticsearch][download][branch] = 7.x-1.x
projects[search_api_elasticsearch][patch][1922420] = https://drupal.org/files/1922420-search_api_elasticsearch-elastica_namespaces.patch
;projects[search_api_elasticsearch][patch][remove-drush] = https://bitbucket.org/asgorobets/es-starterkit-patches/raw/ef9e9af46619f4bf6e22e7e2ecc69c26f623e26b/search_api_elasticsearch-remove-drush-integration.patch

projects[efq_views][version] = 1.x-dev
projects[efq_views][subdir] = contrib
projects[efq_views][download][type] = git
projects[efq_views][download][revision] = fc41a8c
projects[efq_views][download][branch] = 7.x-1.x
projects[efq_views][patch][2070321] = https://drupal.org/files/efq_views-broken_numeric_filter_between_op-2070321.patch
projects[efq_views][patch][1924066] = https://drupal.org/files/efq_views-plain-text-fields-1924066-3.patch

;============================================
;  Libraries
;============================================

; Elastica library
libraries[Elastica][download][type] = git
libraries[Elastica][download][url] = git@github.com:ruflin/Elastica.git
libraries[Elastica][download][tag] = v0.90.1.0
